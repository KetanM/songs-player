package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * Created by samy.messara
 */

interface MVPContract {

    /**
     * Interface for all views.
     * The presenter can be bound only with classes that implements MVPContract.ViewContract
     */
    interface ViewContract {
        /**
         * Get the application context
         */
        fun getViewAppContext() : Context
    }

    /**
     * Interface for all presenters.
     * The view can be bound only with classes that implements MVP.Presenter
     */
    interface PresenterContract {
        /**
         * Bound with [view].onCreate
         */
        fun onCreate(savedInstanceState: Bundle?)

        /**
         * Bound with [view].onStart
         */
        fun onStart()

        /**
         * Bound with [view].onNewIntent
         */
        fun onNewIntent(intent : Intent)

        /**
         * Bound with [view].onPause
         */
        fun onPause()

        /**
         * Bound with [view].onResume
         */
        fun onResume()

        /**
         * Bound with [view].onStop
         */
        fun onStop()

        /**
         * Bound with [view].onDestroy
         */
        fun onDestroy()


        fun setBundle(arguments: Bundle?)

        fun setIntent(intent: Intent?)
    }

}