package com.smess.songsplayer.domain.converters

import com.smess.songsplayer.domain.models.ItunesFeed
import com.smess.songsplayer.domain.models.Song

object ItunesFeedToSong {

    fun feedToSongsList(feed: ItunesFeed) : ArrayList<Song> {
        var songsList: ArrayList<Song> = ArrayList()
        var id = 0
        for (entry in feed.feed.entry){
            if (!entry.imname.label.isNullOrBlank()
                    && !entry.imartist.label.isNullOrBlank()
                    && !entry.imimage[0].label.isNullOrBlank()
                    && !entry.link[1].attributes.href.isNullOrBlank()) {
                songsList.add(Song(id, entry.imname.label, entry.imartist.label, entry.imimage[0].label.replace("55x55", "300x300"), entry.link[1].attributes.href))
                id++
            }
        }
        return songsList
    }
}