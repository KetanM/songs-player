package com.smess.songsplayer.domain.workers.feed

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import com.smess.songsplayer.services.feed.FeedServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FeedWorker(private val countryCode: String, private val callback: FeedWorkerCallback, private val tag: String) : AbstractWorker() {

    private var disposable: Disposable? = null

    private val feedServices by lazy {
        FeedServices.create()
    }

    override fun run() {
        //Make async stuff here

        disposable = feedServices.getTopCharts(countryCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            callback.onFetchTopChartsSuccess(result, tag)
                        },
                        { error ->
                            callback.onWorkerError(error, tag)
                        }
                )

    }
}