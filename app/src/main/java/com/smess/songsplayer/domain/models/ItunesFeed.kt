package com.smess.songsplayer.domain.models
import com.google.gson.annotations.SerializedName



data class ItunesFeed(
		@SerializedName("feed") val feed: Feed
) {
	data class Feed(
			@SerializedName("author") val author: Author,
			@SerializedName("entry") val entry: List<Entry>,
			@SerializedName("updated") val updated: Updated,
			@SerializedName("rights") val rights: Rights,
			@SerializedName("title") val title: Title,
			@SerializedName("icon") val icon: Icon,
			@SerializedName("link") val link: List<Link>,
			@SerializedName("id") val id: Id
	) {
		data class Updated(
				@SerializedName("label") val label: String
		)
		data class Rights(
				@SerializedName("label") val label: String
		)
		data class Link(
				@SerializedName("attributes") val attributes: Attributes
		) {
			data class Attributes(
					@SerializedName("rel") val rel: String,
					@SerializedName("type") val type: String,
					@SerializedName("href") val href: String
			)
		}
		data class Author(
				@SerializedName("name") val name: Name,
				@SerializedName("uri") val uri: Uri
		) {
			data class Name(
					@SerializedName("label") val label: String
			)
			data class Uri(
					@SerializedName("label") val label: String
			)
		}
		data class Entry(
				@SerializedName("im:name") val imname: Imname,
				@SerializedName("im:image") val imimage: List<Imimage>,
				@SerializedName("im:collection") val imcollection: Imcollection,
				@SerializedName("im:price") val imprice: Imprice,
				@SerializedName("im:contentType") val imcontentType: ImcontentType,
				@SerializedName("rights") val rights: Rights,
				@SerializedName("title") val title: Title,
				@SerializedName("link") val link: List<Link>,
				@SerializedName("id") val id: Id,
				@SerializedName("im:artist") val imartist: Imartist,
				@SerializedName("category") val category: Category,
				@SerializedName("im:releaseDate") val imreleaseDate: ImreleaseDate
		) {
			data class Rights(
					@SerializedName("label") val label: String
			)
			data class Link(
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("rel") val rel: String,
						@SerializedName("type") val type: String,
						@SerializedName("href") val href: String
				)
			}
			data class Imname(
					@SerializedName("label") val label: String
			)
			data class Imartist(
					@SerializedName("label") val label: String,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("href") val href: String
				)
			}
			data class Imprice(
					@SerializedName("label") val label: String,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("amount") val amount: String,
						@SerializedName("currency") val currency: String
				)
			}
			data class ImcontentType(
					@SerializedName("im:contentType") val imcontentType: ImcontentType,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("term") val term: String,
						@SerializedName("label") val label: String
				)
				data class ImcontentType(
						@SerializedName("attributes") val attributes: Attributes
				) {
					data class Attributes(
							@SerializedName("term") val term: String,
							@SerializedName("label") val label: String
					)
				}
			}
			data class Imcollection(
					@SerializedName("im:name") val imname: Imname,
					@SerializedName("link") val link: Link,
					@SerializedName("im:contentType") val imcontentType: ImcontentType
			) {
				data class Link(
						@SerializedName("attributes") val attributes: Attributes
				) {
					data class Attributes(
							@SerializedName("rel") val rel: String,
							@SerializedName("type") val type: String,
							@SerializedName("href") val href: String
					)
				}
				data class Imname(
						@SerializedName("label") val label: String
				)
				data class ImcontentType(
						@SerializedName("im:contentType") val imcontentType: ImcontentType,
						@SerializedName("attributes") val attributes: Attributes
				) {
					data class Attributes(
							@SerializedName("term") val term: String,
							@SerializedName("label") val label: String
					)
					data class ImcontentType(
							@SerializedName("attributes") val attributes: Attributes
					) {
						data class Attributes(
								@SerializedName("term") val term: String,
								@SerializedName("label") val label: String
						)
					}
				}
			}
			data class Category(
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("im:id") val imid: String,
						@SerializedName("term") val term: String,
						@SerializedName("scheme") val scheme: String,
						@SerializedName("label") val label: String
				)
			}
			data class Title(
					@SerializedName("label") val label: String
			)
			data class Imimage(
					@SerializedName("label") val label: String,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("height") val height: String
				)
			}
			data class Id(
					@SerializedName("label") val label: String,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("im:id") val imid: String
				)
			}
			data class ImreleaseDate(
					@SerializedName("label") val label: String,
					@SerializedName("attributes") val attributes: Attributes
			) {
				data class Attributes(
						@SerializedName("label") val label: String
				)
			}
		}
		data class Id(
				@SerializedName("label") val label: String
		)
		data class Title(
				@SerializedName("label") val label: String
		)
		data class Icon(
				@SerializedName("label") val label: String
		)
	}
}