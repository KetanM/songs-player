package com.smess.songsplayer.domain.workers.feed

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback
import com.smess.songsplayer.domain.models.ItunesFeed


interface FeedWorkerCallback : BaseWorkerCallback {
    fun onFetchTopChartsSuccess(itunesFeed: ItunesFeed, tag: String)
}