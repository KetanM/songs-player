package com.smess.songsplayer.domain.workers.search

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback
import com.smess.songsplayer.domain.models.ItunesSearch


interface SearchWorkerCallback : BaseWorkerCallback {
    fun onSearchSuccess(itunesSearch: ItunesSearch, tag: String)
}