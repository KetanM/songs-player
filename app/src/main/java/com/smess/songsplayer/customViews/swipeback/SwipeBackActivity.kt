package com.smess.songsplayer.customViews.swipeback

import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.RelativeLayout.LayoutParams
import com.smess.basemvpandroidarchitecture.ui.BaseActivity
import com.smess.basemvpandroidarchitecture.ui.MVPContract


abstract class SwipeBackActivity<T : MVPContract.PresenterContract> : BaseActivity<T>(), SwipeBackLayout.SwipeBackListener {

    var swipeBackLayout: SwipeBackLayout? = null
        private set
    private var ivShadow: ImageView? = null

    private val container: View
        get() {
            val container = RelativeLayout(this)
            swipeBackLayout = SwipeBackLayout(this)
            swipeBackLayout!!.setDragEdge(DEFAULT_DRAG_EDGE)
            swipeBackLayout!!.setOnSwipeBackListener(this)
            ivShadow = ImageView(this)
            ivShadow!!.setBackgroundColor(ContextCompat.getColor(this, android.R.color.black))
            val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
            container.addView(ivShadow, params)
            container.addView(swipeBackLayout)
            return container
        }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(container)
        val view = LayoutInflater.from(this).inflate(layoutResID, null)
        swipeBackLayout!!.addView(view)
    }

    fun setEnableSwipe(enableSwipe: Boolean) {
        swipeBackLayout!!.setEnablePullToBack(enableSwipe)
    }

    fun setDragEdge(dragEdge: SwipeBackLayout.DragEdge) {
        swipeBackLayout!!.setDragEdge(dragEdge)
    }

    fun setDragDirectMode(dragDirectMode: SwipeBackLayout.DragDirectMode) {
        swipeBackLayout!!.setDragDirectMode(dragDirectMode)
    }

    override fun onViewPositionChanged(fractionAnchor: Float, fractionScreen: Float) {
        ivShadow!!.alpha = 1 - fractionScreen
    }

    companion object {

        private val DEFAULT_DRAG_EDGE = SwipeBackLayout.DragEdge.LEFT
    }

}
