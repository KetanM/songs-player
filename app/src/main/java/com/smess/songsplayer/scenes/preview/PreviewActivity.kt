package com.smess.songsplayer.scenes.preview


import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.widget.LinearLayout
import android.widget.SeekBar
import com.smess.songsplayer.R
import com.smess.songsplayer.customViews.swipeback.SwipeBackActivity
import com.smess.songsplayer.customViews.swipeback.SwipeBackLayout
import com.smess.songsplayer.domain.models.Song
import com.smess.songsplayer.scenes.preview.adapter.SongPreviewPagerAdapter
import com.smess.songsplayer.utils.Utils
import kotlinx.android.synthetic.main.activity_preview.*


class PreviewActivity : SwipeBackActivity<PreviewContract.PresenterContract>(), PreviewContract.ViewContract {

    companion object {
        private val TAG = PreviewActivity::class.java.simpleName
        val ARG_SONG_POSITION = "song_position"
        val ARG_SONG_LIST = "song_list"
        val ARG_SONG_PLAYBACK_POSITION = "song_playback_position"
        val ARG_SONG_IS_PLAYING = "song_is_playing"
        val ARG_SONG_CURRENT_POSITION = "song_current_position"
        private const val PREVIEW_DURATION_IN_MILLIS = 30000
        private const val SECONDE_IN_MILLIS = 1000

    }
    private lateinit var durationHandler: Handler

    private lateinit var pagerAdapter: SongPreviewPagerAdapter
    private lateinit var songList: ArrayList<Song>
    private var mediaPlayer: MediaPlayer = MediaPlayer()

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUi(R.layout.activity_preview)
        checkScreenOrientation()
    }

    private fun checkScreenOrientation() {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            llPreviewContainer.orientation = LinearLayout.VERTICAL
            llPreviewThumbContainer.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f)
            llPreviewControlsContainer.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f)

        }
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            llPreviewContainer.orientation = LinearLayout.HORIZONTAL
            llPreviewThumbContainer.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f)
            llPreviewControlsContainer.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f)
        }
    }

    override fun showNextSong() {
        thumbViewPager.currentItem = thumbViewPager.currentItem + 1
    }

    override fun showPreviousSong() {
        thumbViewPager.currentItem = thumbViewPager.currentItem - 1
    }

    override fun prepareMediaPlayer(shouldPlay: Boolean) {
        //Preparing player
        mediaPlayer.reset()
        mediaPlayer.setDataSource(songList[thumbViewPager.currentItem].previewUrl)
        mediaPlayer.prepareAsync()
        mediaPlayer.setOnPreparedListener {
            sbSongProgression.max = mediaPlayer.duration / SECONDE_IN_MILLIS
            if (shouldPlay) {
                it.start()
            }
        }
        mediaPlayer.setOnCompletionListener {
            presenter.onMediaCompleted(thumbViewPager.currentItem, songList.size)
        }
        durationHandler = Handler()
        //Update Seekbar on UI thread
        runOnUiThread(object : Runnable {

            override fun run() {
                try {
                    val currentPosition = mediaPlayer.currentPosition / SECONDE_IN_MILLIS
                    sbSongProgression.progress = currentPosition

                    val passedTime = sbSongProgression.progress * SECONDE_IN_MILLIS
                    val remainingTime = PREVIEW_DURATION_IN_MILLIS - (sbSongProgression.progress * SECONDE_IN_MILLIS)
                    tvPassedTime.text = Utils.getTimeString(passedTime)
                    tvRemainingTime.text = Utils.getTimeString(remainingTime)

                    durationHandler.postDelayed(this, SECONDE_IN_MILLIS.toLong())
                }catch (e : IllegalStateException){
                    e.printStackTrace()
                }
            }
        })
    }

    private fun restorePlayerState(savedInstanceState: Bundle) {
        mediaPlayer.seekTo(savedInstanceState.getInt(ARG_SONG_PLAYBACK_POSITION))
        thumbViewPager.currentItem = savedInstanceState.getInt(ARG_SONG_CURRENT_POSITION)
        if(savedInstanceState.getBoolean(ARG_SONG_IS_PLAYING)){
            mediaPlayer.start()
            showPauseButton()
        }else{
            mediaPlayer.pause()
            showPlayButton()
        }
    }


    override fun updateSongInformations(position: Int) {
        tvSongTitle.text = songList[position].title
        tvSingerName.text = songList[position].author
    }

    private fun bindClicks() {
        ivGoBack.setOnClickListener {
            presenter.onClickGoBack()
        }
        ivPlayPause.setOnClickListener {
            presenter.onClickPlayPauseButton(mediaPlayer.isPlaying)
        }
        ivNextSong.setOnClickListener {
            presenter.onClickNextSongButton(thumbViewPager.currentItem, songList.size)
        }
        ivPreviousSong.setOnClickListener {
            presenter.onClickPreviousSongButton(thumbViewPager.currentItem)
        }
    }

    override fun startPlayer() {
        mediaPlayer.start()
    }

    override fun pausePlayer() {
        mediaPlayer.pause()
        presenter.savePlaybackPosition(mediaPlayer.currentPosition)
    }

    override fun restartPlayer(playbackPosition: Int) {
        mediaPlayer.seekTo(playbackPosition)
        mediaPlayer.start()
    }

    override fun stopPlayer() {
        mediaPlayer.stop()
    }

    override fun showPlayButton() {
        ivPlayPause.setImageDrawable(resources.getDrawable(R.drawable.play_btn, theme))
    }

    override fun showPauseButton() {
        ivPlayPause.setImageDrawable(resources.getDrawable(R.drawable.pause, theme))
    }

    override fun createPresenter(): PreviewContract.PresenterContract {
        return PreviewPresenter(this)
    }

    override fun goBack() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down)
    }

    override fun onDestroy() {
        super.onDestroy()
        durationHandler.removeCallbacksAndMessages(null)
        mediaPlayer.release()
    }

    override fun onSaveInstanceState(state: Bundle) {
        // Save  states
        super.onSaveInstanceState(state)
        state.putInt(ARG_SONG_PLAYBACK_POSITION, mediaPlayer.currentPosition)
        state.putBoolean(ARG_SONG_IS_PLAYING, mediaPlayer.isPlaying)
        state.putInt(ARG_SONG_CURRENT_POSITION, thumbViewPager.currentItem)
    }

    override fun onRestoreInstanceState(state: Bundle?) {
        super.onRestoreInstanceState(state)
        // Retrieve saved states
        if (state != null) {
            restorePlayerState(state)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        checkScreenOrientation()
    }

    private fun setupUi(layoutId: Int){
        setContentView(layoutId)
        setDragEdge(SwipeBackLayout.DragEdge.TOP)
        bindClicks()

        songList = intent.extras[ARG_SONG_LIST] as ArrayList<Song>
        var songPosition = intent.extras.getInt(ARG_SONG_POSITION)

        pagerAdapter = SongPreviewPagerAdapter(supportFragmentManager, songList)
        thumbViewPager.adapter = pagerAdapter
        thumbViewPager.currentItem = songPosition

        thumbViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                presenter.onPageChanged(position, songList.size, mediaPlayer.isPlaying)
                updateSongInformations(thumbViewPager.currentItem)
            }

        })

        prepareMediaPlayer(true)
        updateSongInformations(thumbViewPager.currentItem)

        sbSongProgression.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress * SECONDE_IN_MILLIS)
                }
            }
        })
    }

    override fun getViewAppContext(): Context {
        return this
    }
}
