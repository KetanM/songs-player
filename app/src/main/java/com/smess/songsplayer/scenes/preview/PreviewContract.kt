package com.smess.songsplayer.scenes.preview

import com.smess.basemvpandroidarchitecture.ui.MVPContract


interface PreviewContract {

    interface ViewContract : MVPContract.ViewContract {
        //functions implemented by the view and called from the presenter goes here
        fun goBack()
        fun prepareMediaPlayer(shouldPlay: Boolean)
        fun startPlayer()
        fun pausePlayer()
        fun restartPlayer(playbackPosition: Int)
        fun stopPlayer()
        fun showPlayButton()
        fun showPauseButton()
        fun showNextSong()
        fun showPreviousSong()
        fun updateSongInformations(position: Int)

    }

    interface PresenterContract : MVPContract.PresenterContract {
        //functions implemented by the presenter and called from the view goes here
        fun onClickGoBack()
        fun onClickPlayPauseButton(isPlaying: Boolean)
        fun onClickNextSongButton(currentItem: Int, childCount: Int)
        fun onClickPreviousSongButton(currentItem: Int)
        fun savePlaybackPosition(currentPosition: Int)
        fun onMediaCompleted(currentItem: Int, childCount: Int)
        fun onPageChanged(position: Int, size: Int, isPlaying: Boolean)

    }
}