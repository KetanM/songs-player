package com.smess.songsplayer.scenes.main

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import com.smess.basemvpandroidarchitecture.ui.MVPContract
import com.smess.songsplayer.domain.models.Song
import com.smess.songsplayer.scenes.main.adapter.ListSongsAdapter
import java.util.*


interface MainContract {

    interface ViewContract : MVPContract.ViewContract {
        //functions implemented by the view and called from the presenter goes here
        fun refreshSongsListContent(adapter: ListSongsAdapter)
        fun stopRefreshing()
        fun showProgress()
        fun hideProgress()
        fun showNoDataLayout()
        fun hideNoDataLayout()
        fun showSnackBarMessage(message: String)
        fun hideSongsList()
        fun showSongsList()
        fun smoothScrollToTop()
        fun hideFab()
        fun showFab()
        fun getContext(): Context
        fun getSearchViewContent(): String
        fun startVoiceCapture()
        fun clearMainSearchView()
        fun updateSearchRightButtonDrawable(resId: Int)
        fun startRequestPermissions(permsList: Array<String>, requestCode: Int)
        fun goToPreviewActivity(songPosition: Int, songList: ArrayList<Song>)
    }

    interface PresenterContract : MVPContract.PresenterContract, SwipeRefreshLayout.OnRefreshListener {
        //functions implemented by the presenter and called from the view goes here
        fun onSearchTextChange(searchTerms: CharSequence)
        fun fetchTopCharts()
        fun onClickFabScrollTop()
        fun onScrollStateChanged(recyclerView: RecyclerView)
        fun onSearchSubmitted(terms: String) : Boolean
        fun onClickSearchRightAction(searchText: String)
        fun onVoiceSearchError(code: Int)
        fun onClickSearchLeftAction(searchText: String)
    }
}