package com.smess.songsplayer.services.feed

import com.smess.songsplayer.BuildConfig
import com.smess.songsplayer.domain.models.ItunesFeed
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit


/**
 * Created by samy.messara
 */
interface FeedServices {

    @GET("{countryCode}/rss/topsongs/limit=100/json")
    fun getTopCharts(@Path("countryCode") countryCode: String): Observable<ItunesFeed>

    companion object {
        private const val URL = BuildConfig.ITUNES_SERVICE_BASE_ULR

        fun create(): FeedServices {

            val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            }

            val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL)
                    .client(okHttpClient)
                    .build()

            return retrofit.create(FeedServices::class.java)
        }
    }
}